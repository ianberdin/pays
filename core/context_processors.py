# -*- coding: utf-8 -*-
from random import random

from django.contrib.sites.models import Site


def site(request):
    return {'site': Site.objects.get_current()}


def mobile_browser(request):
    dict = {'mobile_browser': False}
    if hasattr(request, 'is_mobile'):
        dict['mobile_browser'] = request.is_mobile
    return dict