# -*- coding: utf-8 -*-
import re

from django.template import Library

register = Library()


@register.filter
def rupluralize(value, arg="дурак,дурака,дураков"):
    args = arg.split(",")
    number = abs(int(value))
    a = number % 10
    b = number % 100

    if (a == 1) and (b != 11):
        return args[0]
    elif (a >= 2) and (a <= 4) and ((b < 10) or (b >= 20)):
        return args[1]
    else:
        return args[2]


@register.filter
def replace(string, args):
    search = args.split(args[0])[1]
    replace = args.split(args[0])[2]

    return re.sub(search, replace, string)


def paginator(context, adjacent_pages=2):
    """
    Сокращение размера пагинации и её отображение.
    Например: 1 2 3 ... 9 10
    """
    paged = context['page_obj']
    paginator = paged.paginator
    start_page = max(paged.number - adjacent_pages, 1)
    if start_page <= 3:
        start_page = 1
    end_page = paged.number + adjacent_pages + 1
    if end_page >= paginator.num_pages - 1:
        end_page = paginator.num_pages + 1
    page_numbers = [n for n in range(start_page, end_page) if 0 < n <= paginator.num_pages]

    data = {
        'paged': paged,
        'paginator': paginator,
        'page': paged.number,
        'pages': paginator.num_pages,
        'page_numbers': page_numbers,
        'next': paged.next_page_number() if paged.has_next() else None,
        'previous': paged.previous_page_number() if paged.has_previous() else None,
        'has_next': paged.has_next(),
        'has_previous': paged.has_previous(),
        'show_first': 1 not in page_numbers,
        'show_last': paginator.num_pages not in page_numbers,
        'is_paginated': context['is_paginated'],

    }
    if 'request' in context:
        getvars = context['request'].GET.copy()
        if 'page' in getvars:
            del getvars['page']
        if len(getvars.keys()) > 0:
            data['getvars'] = "&%s" % getvars.urlencode()
        else:
            data['getvars'] = ''

    return data


register.inclusion_tag('core/utils/_paginator.html', takes_context=True)(paginator)