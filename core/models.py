# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from easy_thumbnails.fields import ThumbnailerImageField


class Project(models.Model):
    name = models.CharField('Имя проекта', max_length=200)
    description_sm = models.CharField('Небольшое описание', max_length=290)
    description = models.TextField('Полное описание', max_length=2000)
    logo = ThumbnailerImageField(verbose_name='Лого проекта', blank=True, null=True, upload_to='logos/')
    prezi = models.FileField('Презентация', max_length=200, blank=True, null=True, upload_to="prezi/")
    site = models.CharField('Ссылка на сайт', max_length=200, blank=True, null=True)

    orders = models.ManyToManyField('core.Order', blank=True, null=True, verbose_name='Заявки', related_name='project_orders')

    want_money = models.IntegerField('Сколько денег надо', default=0)
    give_percent = models.IntegerField('Процент от компании', default=0)
    multi_hand = models.BooleanField('Несколько паев в одни руки', default=True)
    pays = models.IntegerField('Количество паев', default=1)

    is_hidden = models.BooleanField('Скрыть', default=False)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Проект'
        verbose_name_plural = 'Проекты'

    def __unicode__(self):
        return self.name

    def get_count_hands(self):
        return sum(self.orders.all().values_list('count', flat=True))

    def get_residue_pays(self):
        return self.pays - self.get_count_hands()


class Order(models.Model):
    project = models.ForeignKey(Project, verbose_name='Проект')
    user = models.ForeignKey('users.User', verbose_name='Пользователь')
    count = models.IntegerField('Количество паев', default=1)

    class Meta:
        verbose_name = 'Заявка'
        verbose_name_plural = 'Заявки'

    def __unicode__(self):
        return "%s - %s: %s" % (self.user.email, self.project.name, self.count)


class Comment(models.Model):
    user = models.ForeignKey("users.User", verbose_name='Пользователь')
    project = models.ForeignKey(Project, verbose_name='Проект')
    body = models.TextField('Текст комментария', max_length=2000)
    created = models.DateTimeField('Дата создания', auto_now_add=True)

    class Meta:
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Комментарии'

    def __unicode__(self):
        return "%s - %s: %s" % (self.user.email, self.project.name, self.body[:50])
