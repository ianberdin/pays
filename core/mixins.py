# coding: UTF-8
from django.utils.decorators import method_decorator


def class_view_decorator(function_decorator):
    def deco(View):
        View.dispatch = method_decorator(function_decorator)(View.dispatch)
        return View
    return deco