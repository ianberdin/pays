# coding: utf-8
import datetime
import itertools

from django.http import HttpResponseRedirect
from django.utils.dateformat import format as _dateformat, time_format as _timeformat
from django.conf import settings


def dateformat(date, human=False):
    if human:
        return _dateformat(date, 'd F, l')
    return _dateformat(date, 'd.m.Y')


def datetimeformat(date, human=False):
    if human:
        return "%s %s" % (_dateformat(date, 'd F, l'), _timeformat(date, 'H:i'))
    return "%s %s" % (_dateformat(date, 'd F Y'), _timeformat(date, 'H:i'))


def timeformat(date):
    return _timeformat(date, 'H:i')


def split_datetime(date):
    return [dateformat(date), timeformat(date)]


def get_object_or_none(klass, *args, **kwargs):
    try:
        return klass._default_manager.get(*args, **kwargs)
    except klass.DoesNotExist:
        return None


def regroup_by_created(lst):
    grouped = []
    for key, group in itertools.groupby(lst, key=lambda x: x.get_format_created()):
        grouped.append(key)
        for task in group:
            grouped.append(task)
    return grouped


def validate_email(email):
    from django.core.validators import validate_email
    from django.core.exceptions import ValidationError

    try:
        validate_email(email)
        return True
    except ValidationError:
        return False


def custom_redirect(url_name, *args, **kwargs):
    from django.core.urlresolvers import reverse
    import urllib

    url = reverse(url_name, args=args)
    params = urllib.urlencode(**kwargs)
    return HttpResponseRedirect(url + "?%s" % params)


def set_cookie(response, key, value, days_expire=7):
    if days_expire is None:
        max_age = 365 * 24 * 60 * 60  # one year
    else:
        max_age = days_expire * 24 * 60 * 60
    expires = datetime.datetime.strftime(datetime.datetime.utcnow() + datetime.timedelta(seconds=max_age),
                                         "%a, %d-%b-%Y %H:%M:%S GMT")
    response.set_cookie(key, value, max_age=max_age, expires=expires, domain=settings.SESSION_COOKIE_DOMAIN,
                        secure=settings.SESSION_COOKIE_SECURE or None)


def rupluralize_num(value, arg="дурак,дурака,дураков"):
    args = arg.split(",")
    number = abs(int(value))
    a = number % 10
    b = number % 100

    if (a == 1) and (b != 11):
        return '%s %s' % (value, args[0])
    elif (a >= 2) and (a <= 4) and (b < 10) or (b >= 20):
        return '%s %s' % (value, args[1])
    else:
        return '%s %s' % (value, args[2])