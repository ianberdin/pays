# -*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.views import generic
from django.views.generic.edit import FormMixin

from .models import Project, Comment, Order
from .forms import CommentForm, OrderForm
from .mixins import class_view_decorator

@class_view_decorator(login_required)
class ProjectListView(generic.ListView):
    queryset = Project.objects.filter(is_hidden=False)
    paginate_by = 10


@class_view_decorator(login_required)
class ProjectDetailView(generic.DetailView, FormMixin):
    model = Project
    form_class = CommentForm

    def get_success_url(self):
        return reverse('core:project-detail', kwargs={'pk': self.object.pk})

    def get_context_data(self, **kwargs):
        context = super(ProjectDetailView, self).get_context_data(**kwargs)
        context['comments'] = Comment.objects.filter(project=self.object.pk)
        form_class = self.get_form_class()
        context['form'] = self.get_form(form_class)
        context['order_form'] = OrderForm()
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()

        if 'count' in request.POST:
            order = Order.objects.create(
                user=self.request.user,
                project=self.object,
                count=int(self.request.POST.get('count', None)),
            )
            self.object.orders.add(order)

        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            Comment.objects.create(
                project=self.object,
                user=self.request.user,
                body=form.cleaned_data.get('body')
            )
            return self.form_valid(form)
        else:
            return self.form_invalid(form)