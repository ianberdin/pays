from django.contrib import admin

from .models import Project, Comment, Order


class ProjectAdmin(admin.ModelAdmin):
    list_display = ('name', 'description_sm')

    class Media:
        js = [
            '/static/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js',
            '/static/grappelli/tinymce_setup/tinymce_setup.js',
        ]


admin.site.register(Project, ProjectAdmin)


class CommentAdmin(admin.ModelAdmin):
    list_filter = ('user', 'project')
    search_fields = ('body',)


admin.site.register(Comment, CommentAdmin)


class OrderAdmin(admin.ModelAdmin):
    list_filter = ('user', 'project')


admin.site.register(Order, OrderAdmin)