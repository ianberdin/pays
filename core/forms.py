# coding: utf-8
from django import forms

from .models import Comment, Order


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('body',)


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        field = ('count',)