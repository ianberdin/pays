# -*- coding: utf-8 -*-
import datetime
from django.core.cache import cache
from django.conf import settings


class ActiveUserMiddleware:
    def process_request(self, request):
        current_user = request.user
        if current_user.is_authenticated():
            now = datetime.datetime.now()
            cache.set('seen_%s' % (current_user), now,
                      settings.USER_LASTSEEN_TIMEOUT)