# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_auto_20140922_1205'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='comment',
            options={'verbose_name': '\u041a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439', 'verbose_name_plural': '\u041a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0438'},
        ),
        migrations.AlterModelOptions(
            name='order',
            options={'verbose_name': '\u0417\u0430\u044f\u0432\u043a\u0430', 'verbose_name_plural': '\u0417\u0430\u044f\u0432\u043a\u0438'},
        ),
        migrations.AlterModelOptions(
            name='project',
            options={'verbose_name': '\u041f\u0440\u043e\u0435\u043a\u0442', 'verbose_name_plural': '\u041f\u0440\u043e\u0435\u043a\u0442\u044b'},
        ),
        migrations.AlterField(
            model_name='project',
            name='description_sm',
            field=models.CharField(max_length=290, verbose_name='\u041d\u0435\u0431\u043e\u043b\u044c\u0448\u043e\u0435 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435'),
        ),
    ]
