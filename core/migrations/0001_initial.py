# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import easy_thumbnails.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200, verbose_name='\u0418\u043c\u044f \u043f\u0440\u043e\u0435\u043a\u0442\u0430')),
                ('description_sm', models.TextField(max_length=350, verbose_name='\u041d\u0435\u0431\u043e\u043b\u044c\u0448\u043e\u0435 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435')),
                ('description', models.TextField(max_length=2000, verbose_name='\u041f\u043e\u043b\u043d\u043e\u0435 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435')),
                ('logo', easy_thumbnails.fields.ThumbnailerImageField(upload_to=b'', null=True, verbose_name='\u041b\u043e\u0433\u043e \u043f\u0440\u043e\u0435\u043a\u0442\u0430', blank=True)),
                ('created', models.DateTimeField(auto_now=True)),
                ('prezi', models.FileField(max_length=200, upload_to='prezi/', null=True, verbose_name='\u041f\u0440\u0435\u0437\u0435\u043d\u0442\u0430\u0446\u0438\u044f', blank=True)),
                ('site', models.TextField(max_length=200, null=True, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430 \u043d\u0430 \u0441\u0430\u0439\u0442', blank=True)),
                ('want_money', models.IntegerField(default=0, verbose_name='\u0421\u043a\u043e\u043b\u044c\u043a\u043e \u0434\u0435\u043d\u0435\u0433 \u043d\u0430\u0434\u043e')),
                ('give_percent', models.IntegerField(default=0, verbose_name='\u041f\u0440\u043e\u0446\u0435\u043d\u0442 \u043e\u0442 \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u0438')),
                ('multi_hand', models.BooleanField(default=True, verbose_name='\u041d\u0435\u0441\u043a\u043e\u043b\u044c\u043a\u043e \u043f\u0430\u0435\u0432 \u0432 \u043e\u0434\u043d\u0438 \u0440\u0443\u043a\u0438')),
                ('pays', models.IntegerField(default=1, verbose_name='\u041a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e \u043f\u0430\u0435\u0432')),
                ('is_hidden', models.BooleanField(default=False, verbose_name='\u0421\u043a\u0440\u044b\u0442\u044c')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
