# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import easy_thumbnails.fields


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20140922_0904'),
    ]

    operations = [
        migrations.AlterField(
            model_name='project',
            name='created',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='logo',
            field=easy_thumbnails.fields.ThumbnailerImageField(upload_to='logos/', null=True, verbose_name='\u041b\u043e\u0433\u043e \u043f\u0440\u043e\u0435\u043a\u0442\u0430', blank=True),
        ),
    ]
