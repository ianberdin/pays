# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='investors',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL, null=True, verbose_name='\u0418\u043d\u0432\u0435\u0441\u0442\u043e\u0440\u044b', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='project',
            name='site',
            field=models.CharField(max_length=200, null=True, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430 \u043d\u0430 \u0441\u0430\u0439\u0442', blank=True),
        ),
    ]
