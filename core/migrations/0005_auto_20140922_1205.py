# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('core', '0004_comment'),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('count', models.IntegerField(default=1, verbose_name='\u041a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e \u043f\u0430\u0435\u0432')),
                ('project', models.ForeignKey(verbose_name='\u041f\u0440\u043e\u0435\u043a\u0442', to='core.Project')),
                ('user', models.ForeignKey(verbose_name='\u041f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044c', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='project',
            name='investors',
        ),
        migrations.AddField(
            model_name='project',
            name='orders',
            field=models.ManyToManyField(related_name='project_orders', null=True, verbose_name='\u0417\u0430\u044f\u0432\u043a\u0438', to='core.Order', blank=True),
            preserve_default=True,
        ),
    ]
