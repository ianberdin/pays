# coding: utf-8
from django.conf.urls import patterns, url
from django.views.decorators.http import require_POST

from .views import ProjectListView, ProjectDetailView

urlpatterns = patterns('',
                       url(r'^$', ProjectListView.as_view(), name='project-list'),
                       url(r'^project/(?P<pk>\d+)$', ProjectDetailView.as_view(), name='project-detail'),
)
