# -*- coding: utf-8 -*-
import os

try:
    from settings import *
except ImportError:
    print 'NOT settings.py'

PROJECT_DIR = os.path.dirname(os.path.dirname(__file__))

ADMINS = (
    ('happierall', 'happierall@gmail.com'),
)

NOTIFY_DEBUG = DEBUG

MANAGERS = ADMINS

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'compressor.finders.CompressorFinder'
)

INTERNAL_IPS = ('127.0.0.1',)

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.request',
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.debug',
    'django.contrib.messages.context_processors.messages',
    'core.context_processors.site',
    'core.context_processors.mobile_browser',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.middleware.gzip.GZipMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'core.middleware.DetectMobileBrowser',
    'core.activeuser_middleware.ActiveUserMiddleware',
)

try:
    MIDDLEWARE_CLASSES += DEBUG_MIDDLEWARE_CLASSES
except NameError:
    pass

EMAIL_HOST_USER = 'happierall'
ROOT_URLCONF = 'project.urls'

WSGI_APPLICATION = 'project.wsgi.application'

TEMPLATE_DIRS = (
    os.path.join(PROJECT_DIR, 'templates'),
)

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    # 'users.authBackend.AuthBackend'
)

LOGIN_URL = '/login'
LOGIN_ERROR_URL = '/login/error/'

AUTH_USER_MODEL = 'users.User'

ALLOWED_HOSTS = ['*']

INSTALLED_APPS = (
    'compressor',
    'grappelli',
    'easy_thumbnails',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize'
)

MY_APPS = (
    'users',
    'core',
)
INSTALLED_APPS += MY_APPS

try:
    INSTALLED_APPS += DEBUG_APPS
except NameError:
    print 'Not debug apps.'

TIME_ZONE = 'Asia/Yekaterinburg'

from django.contrib.messages import constants as messages

MESSAGE_TAGS = {
    messages.INFO: 'alert-info',
    messages.SUCCESS: 'alert-success',
    messages.WARNING: 'alert-warning',
    messages.ERROR: 'alert-error',
}

SITE_ID = 1

USE_I18N = True

USE_L10N = True

# USE_TZ = True

FAVICON_PATH = STATIC_URL + 'img/favicon.png'

LOGIN_URL = '/login'

ugettext = lambda s: s

LANGUAGE_CODE = 'RU-ru'

LANGUAGES = (
    ('ru', ugettext('Russian')),
)

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'default-cache'
    }
}

# Number of seconds of inactivity before a user is marked offline
USER_ONLINE_TIMEOUT = 300

# Number of seconds that we will keep track of inactive users for before
# their last seen is removed from the cache
USER_LASTSEEN_TIMEOUT = 60 * 60 * 24 * 7