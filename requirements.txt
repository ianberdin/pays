Django==1.7
easy-thumbnails==2.1
django_compressor==1.4
django-grappelli==2.6.1
psycopg2==2.5.4