# -*- coding: utf-8 -*-

from django.shortcuts import render, redirect, get_object_or_404, HttpResponse
from django.core.urlresolvers import reverse
from django.contrib.auth.views import logout as auth_logout
from django.contrib.auth import login as logon
from django.contrib.auth.backends import ModelBackend


def login(request):
    data = {}
    NEXT = request.GET.get('next')
    if NEXT:
        data['next'] = NEXT
    if request.method == "POST":
        next_url = request.POST.get('next')

        if request.user.is_authenticated():
            message = u'Вы уже авторизованы'
        else:
            email = request.POST.get('email')
            password = request.POST.get('password')
            backend = ModelBackend()
            user = backend.authenticate(email=email.lower(), password=password)
            if user is not None:
                user.backend = 'django.contrib.auth.backends.ModelBackend'
                if user.is_active:
                    logon(request, user)
                    if next_url:
                        return redirect(next_url)
                    return redirect(reverse('core:project-list'))
                else:
                    message = u'Ваш аккаунт неактивен'
            else:
                if next_url:
                    data['next'] = next_url
                message = u'Пожалуйста правильность введенных данных'
        try:
            data['message'] = message
        except UnboundLocalError:
            pass
    return render(request, 'users/login.html', data)


def logoutAction(request, next_page='/'):
    language = None
    if hasattr(request, 'session'):
        if 'django_language' in request.session:
            language = request.session['django_language']
    response = auth_logout(request, next_page)
    if language:
        request.session['django_language'] = language
    return response