# coding=utf-8
from __future__ import unicode_literals
import copy
from django.contrib import admin
from django.contrib.auth.models import Group as GroupOld
from django.contrib.auth.admin import GroupAdmin as GroupAdminOld

from .models import User, Group
from .forms import ProfileAdminForm


class UserAdmin(admin.ModelAdmin):
    form = ProfileAdminForm
    list_display = ('email', 'phone', 'first_name', 'last_name', 'created',)
    fieldsets = [
        (None, {
            'fields': [
                'email', 'phone', 'last_name', 'first_name',
                'last_login']
        }),
        ('Изменение пароля пользователя', {
            'classes': ('grp-collapse grp-closed',),
            'fields': ('password1', 'password2')
        }),
    ]

    def save_model(self, request, obj, form, change):
        password = form.cleaned_data.get('password1')
        if password:
            obj.set_password(password)

        super(UserAdmin, self).save_model(request, obj, form, change)

    def get_fieldsets(self, request, obj=None):
        fieldsets = copy.deepcopy(self.declared_fieldsets)
        permission_fields = ['is_active']  # -- don't hide active checkbox, if not is_superuser else []
        if request.user.is_superuser:
            permission_fields += ['is_admin', 'is_superuser', 'groups', 'user_permissions']

        if permission_fields:
            fieldsets.insert(4, (
                'Дополнительные настройки групп и администрирования', {
                    'classes': ('grp-collapse grp-closed',),
                    'fields': permission_fields
                })
            )
        return fieldsets


admin.site.register(User, UserAdmin)


class GroupAdmin(GroupAdminOld):
    list_display = ('name',)


admin.site.unregister(GroupOld)

admin.site.register(Group, GroupAdmin)

