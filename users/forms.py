# coding: utf-8
from django import forms
from django.contrib.auth import get_user_model

User = get_user_model()


class LoginForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('email', 'password')


class ProfileAdminForm(forms.ModelForm):
    password1 = forms.CharField(label='Пароль', required=False,
                                widget=forms.PasswordInput)
    password2 = forms.CharField(label='Повторите пароль', required=False,
                                widget=forms.PasswordInput)

    class Meta:
        model = User

    def clean_password2(self):
        cleaned_data = self.cleaned_data
        password1 = cleaned_data.get('password1')
        password2 = cleaned_data.get('password2')
        if password1 or password2:
            if not password1 == password2:
                raise forms.ValidationError('Введенные пароли не совпадают.')