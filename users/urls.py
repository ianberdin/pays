# coding: utf-8
from django.conf.urls import patterns, url, include
from django.views.generic import TemplateView


urlpatterns = patterns('',
                       url(r'^login/$', 'users.views.login', name='login'),
                       url(r'^logout/$', 'django.contrib.auth.views.logout', name='logout'),
)