# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import datetime
import re

from django.conf import settings
from django.contrib import auth
from django.db import models
from django.core.cache import cache
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.utils.text import capfirst
from django.core.mail import send_mail

from core.utils import validate_email

try:
    from django.utils.timezone import now as datetime_now
except ImportError:
    datetime_now = datetime.datetime.now

SHA1_RE = re.compile('^[a-f0-9]{40}$')


class UserManager(BaseUserManager):
    def create_user(self, email, password, **extra_fields):
        if not email:
            raise ValueError('Users must have an username')

        user = self.model(
            email=email,
            **extra_fields
        )
        user.set_password(password)
        user.backend = 'django.contrib.auth.backends.ModelBackend'
        user.save(using=self._db)

        return user

    def create_superuser(self, password, email, **extra_fields):
        user = self.create_user(
            email=email,
            password=password,
            **extra_fields
        )
        user.is_active = True
        user.is_admin = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser, PermissionsMixin):
    objects = UserManager()

    email = models.EmailField(u'Email', db_index=True, unique=True)
    phone = models.CharField(u'Phone', max_length=30, blank=True, null=True)
    username = models.CharField(u'Юзернейм', max_length=71, blank=True, null=True)
    first_name = models.CharField(verbose_name=u'Имя', max_length=50)
    last_name = models.CharField(verbose_name=u'Фамилия', blank=True, null=True, max_length=50)

    is_active = models.BooleanField(u'Активен', default=False)
    is_admin = models.BooleanField(u'Персонал', default=False)
    created = models.DateTimeField(u'Дата создания', auto_now_add=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'
        ordering = ['created']

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.first_name and self.last_name:
            self.first_name = capfirst(self.first_name.lower())
            self.last_name = capfirst(self.last_name.lower())
        super(User, self).save(force_insert, force_update, using, update_fields)

    def get_full_name(self):
        if self.first_name == self.last_name:
            return self.get_short_name()
        return ' '.join([self.first_name, self.last_name])

    def get_short_name(self):
        return self.first_name

    def __unicode__(self):
        return self.email

    @property
    def is_staff(self):
        return self.is_admin

    def is_authenticated(self):
        return True

    def email_user(self, subject, message, from_email):
        if validate_email(self.email):
            send_mail(subject, message, from_email, [self.email])

    def last_login_strftime(self):
        if self.last_login:
            return self.last_login.strftime('%d/%m/%Y %H:%M')
        return ''

    def last_seen(self):
        return cache.get('seen_%s' % self)

    def is_online(self):
        if self.last_seen():
            now = datetime.datetime.now()
            if now > self.last_seen() + datetime.timedelta(seconds=settings.USER_ONLINE_TIMEOUT):
                return False
            else:
                return True
        else:
            return False


class Group(auth.models.Group):
    class Meta:
        proxy = True
        verbose_name = 'группа пользователей'
        verbose_name_plural = 'группы пользователей'